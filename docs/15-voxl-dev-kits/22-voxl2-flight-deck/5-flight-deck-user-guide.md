---
layout: default
title: VOXL 2 Flight Deck User Guide
parent: VOXL 2 Flight Deck
nav_order: 5
has_children: true
permalink: /voxl2-flight-deck-userguide/
---

# VOXL 2 Flight Deck User Guide
{: .no_toc }

This guide walks you through the main connections of the VOXL 2 Flight Deck in effort to assist you getting it setup for a vehicle.

For technical details, see the [datasheet](/voxl2-flight-deck-datasheet/).

![voxl2-flight-deck](/images/voxl2-flight-deck/voxl2-deck-hero.png)
