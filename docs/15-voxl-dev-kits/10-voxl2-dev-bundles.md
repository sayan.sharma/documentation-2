---
layout: default
title: VOXL 2 Dev Bundles
parent: VOXL Dev Kits
nav_order: 10
permalink: /voxl2-development-kits/
thumbnail: /voxl2/m0054-c3-kit.png
buylink: https://www.modalai.com/products/voxl-2
summary: VOXL 2 Dev Bundles
---

# VOXL 2 Dev Bundles
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

### Summary

VOXL2 comes in a variety of development kits as listed below and available [here](https://www.modalai.com/voxl-2).


| Part Number                    | Description  |
|---                             |---           |
| `MDK-M0054-1-00`               | VOXL 2 Dev Kit, Board Only |
| `MDK-M0054-1-01`               | VOXL 2 Dev Kit, Power Module and Supply, No Cameras  |
| `MDK-M0054-1-01-C3`            | VOXL 2 Dev Kit, 4 Image Sensors (Front Stereo, Tracking, Hires)  |
| `MDK-M0054-1-01-C11`           | VOXL 2 Dev Kit, 6 Image Sensors (Front/Rear Stereo, Tracking, Hires)  |

| Part Number                    | Description  |
|---                             |---           |
| `MDK-F0006-1-01`               | VOXL 2 Flight Deck with Microhard Modem Carrier Board and USB Hub |
| `MDK-F0006-1-02`               | VOXL 2 Flight Deck with Microhard Modem Carrier Board and USB Hub |
| `MDK-F0006-1-03`               | VOXL 2 Flight Deck with Microhard Modem Carrier Board and USB Hub |

| Part Number                    | Description  |
|---                             |---           |
| `MRB-D0006-4-V1-C11-M7-T1-K0`  | Sentinel Development Drone Kit, with 5G Modem Add-On |
| `MRB-D0006-4-V1-C11-M13-T1-K0` | Sentinel Development Drone Kit, with Wi-Fi Modem Add-On |
| `MRB-D0006-4-V1-C11-M3-T1-K0`  | Sentinel Development Drone Kit, with Microhard Modem Add-On |
| `MRB-D0006-4-V1-C11`           | Sentinel Development Drone, Stand Alone |


### MDK-M0054-1-00

This kit contains the VOXL2 board only, and nothing else.  Volume users or users looking for a replacement would likely choose this development kit.

### MDK-M0054-1-01

This kit contains the VOXL2 board, along with the VOXL Power Module v3, power supply, and supporting cable.  Users looking to start out with development but don't need cameras are likely to choose this development kit.

### MDK-M0054-1-C3

<img src="/images/voxl2/m0054-c3-kit.png" alt="m0054-c3-kit" width="1280"/>

This kit contains the same as `MDK-M0054-1-01`, but adds front stereo, tracking and hires image sensors.  This kit is likely to be chosen if you want to use the computer vision features of the system and obtain functionality like on our M500 development drone.

### MDK-M0054-1-C11

<img src="/images/voxl2/m0054-c11-kit.png" alt="m0054-c11-kit" width="1280"/>

This kit contains the same as `MDK-M0054-1-03`, but also adds rear stereo image senors as well.

### MDK-F0006-1

### MDK-F0006-1-M7

<img src="/images/voxl2/f0006-m7-kit.png" alt="f0006-m7-kit" width="1280"/>

### MDK-F0006-1-M13

<img src="/images/voxl2/f0006-m13-kit.png" alt="f0006-m13-kit" width="1280"/>

### MDK-F0006-1-M3

<img src="/images/voxl2/f0006-m3-kit.png" alt="f0006-m3-kit" width="1280"/>

### MRB-D0006-4-V1-C11

<img src="/images/voxl2/d0006-front.png" alt="d0006-front" width="1280"/>

### MRB-D0006-4-V1-C11-M7-KO

<img src="/images/voxl2/d0006-m7-kit.png" alt="d0006-m7-kit" width="1280"/>

### MRB-D0006-4-V1-C11-M13-KO

<img src="/images/voxl2/d0006-m13-kit.png" alt="d0006-m13-kit" width="1280"/>

### MRB-D0006-4-V1-C11-M3-KO

<img src="/images/voxl2/d0006-m3-kit.png" alt="d0006-m3-kit" width="1280"/>

[Next: VOXL 2 ESC Safety](/voxl2-esd-safety/){: .btn .btn-green }
