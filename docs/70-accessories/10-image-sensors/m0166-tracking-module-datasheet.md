---
layout: default
title: M0166 Tracking Module Datasheet
parent: Image Sensors
nav_order: 166
has_children: false
permalink: /M0166/
---

# VOXL 2 Tracking Sensor Datasheet
{: .no_toc }

## MSU-M0166-1-01


{:toc}

### Specification

| Specification | Value                                                                                |
|----------------|--------------------------------------------------------------------------------------|
| Sensor         | OnSemi AR0144 [Datasheet]() |
| Shutter        | Global                                                                               |
| Resolution     | 1280x800                                                                             |
| Framerate      |                                      |
| Data formats   |                                                                       |
| Lens Size      |                                                                                |
| Focusing Range |  |
| Focal Length   |                                                                                 |
| F Number       |                                                                                    |
| Fov(DxHxV)     |                                                                     |
| TV Distortion  |                                                                                |


### Technical Drawings

#### 3D STEP File

#### 2D Diagram

### Pin Out


## Image Samples for Sensor

### Indoor

### Outdoor

## VOXL 2 Integration

SDK 1.3.0 and greater

More information [here](/voxl2-camera-configs/)