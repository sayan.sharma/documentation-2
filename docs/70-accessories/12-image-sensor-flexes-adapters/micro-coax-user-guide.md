---
layout: default
title: Micro-Coaxial User Guide
parent: Image Sensor Flex Cables and Adapters
nav_order: 200
has_children: false
nav_exclude: true
search_exclude: true
permalink: /micro-coax-user-guide/
---

# Micro-Coaxial User Guide

## Overview

Micro coaxial cables speed development by offering a flexible image image sensor connection with little to no NRE time required. Their flexibility also makes them an ideal choice for gimbal applications. Our Micro coaxial cables are only 2mm in diameter, have a bend radius of 1.5mm, and are available off the shelf in a variety of lengths from 35mm to 180mm.

Users should note that MIPI lines are uncoupled over the length of a coaxial cable and therefore have less noise immunity. Users may notice increased noise and decreased performance, especially at high speeds, when compared to flex cables. The connectors for coaxial cables are also less rugged than flex cable connectors, and are only rated for 30 insertions.

## Image Sensor Cable User Code

The Image Sensor Cable User Code is intended to make it simpler to connect image sensors using coax cables to VOXL 2 and VOXL 2 Mini (Host Boards). Coaxial cables have a limited number of pins, which requires different pinouts to be used for different types of sensors. Connecting boards with 2 different pinouts could damage sensors, adapters, or even host boards, so the image sensor cable user code was created to help reduce the chances of improper board connections. Adapter boards for coaxial cables are color coded to provide a simple visual indicator of board compatibility.

![Image_Sensor_Cable_User_Code](/images/other-products/image-sensors/Image_Sensor_Cable_User_Code_2024-05-16.jpg)

## Adapter Compatibility

**Board Connection ①** = S adapters must be paired with a **Board Connection ①** ≠ S adapter. **Board Connection ①** ≠ S adapters must be paired with a **Board Connection ①** = S or **①** = EXT adapter. Connected adapters must have matching **Cable Connectors ③** and **MIPI/Power Configs ④**.

### Compatibility Matrix
![Image_Sensor_Compatibility_Matrix](/images/other-products/image-sensors/Image_Sensor_Code_Compatibility_Matrix_2024-05-16.jpg)

### Boards with Multiple Image Sensor Adapters
Some Host Side Adapter boards have adapters for **MIPI/Power Config ④** = HR image sensors and adapters for other types of sensors. When multiple types of **MIPI/Power Configs** are on a board, each adapter is labeled with it’s own image sensor cable code and the label is surrounded by a box with the appropriate color for it’s **MIPI/Power Config**. This label may be around the adapter, or opposite the adapter on the reverse side of the board.

Host Side Adapter Boards with **Board Connection ①** = V2 that connect to only one camera group adapter on the Host are compatible with either the VOXL 2 or the VOXL 2 Mini. However, Host Side Adapter Boards that connect to multiple camera group adapters on the host (M0173) are only compatible with their designated Host.