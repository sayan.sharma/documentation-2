---
layout: default
title: VOXL FPV ESC 4-in-1 Datasheet
parent: VOXL ESCs
nav_order: 3
permalink: /voxl-fpv-esc-datasheet/
---

# VOXL FPV Racing 4-in-1 ESC Datasheet
{: .no_toc }

---

## Hardware Overview

This M0138-1 high-performance ESC uses 100A 40V MOSFETs and board layout optimized for demanding FPV racing applications.
The ESC supports 100A burst current per channel and over 300A total burst current (usually limited by battery and connectors).
Communicate reliably with low-latency digital UART from your flight controller to the 4-in-1 ESC.

This ESC has a dedicated 5V regulated power output to power [VOXL 2](/voxl-2/) or [Flight Core v2](/flight-core-v2/)  with battery voltage and current monitoring support in PX4. Additionally the ESC can power a video transmitter and has the ability to toggle on and off.

![m0138_diagram.jpg](/images/modal-esc/m0138/m0138_diagram.jpg)

## Dimensions

![m0138-dimensions.png](/images/modal-esc/m0138/m0138-dimensions.png)

[M0138 VOXL Racing 4-in-1 ESC 3D CAD](https://storage.googleapis.com/modalai_public/modal_drawings/M0138_RACING_ESC_4_IN_1_REVA.step)
- Board dimensions: 45.5mm x 59.0mm
- Mounting hole pattern: 30.5mm x 30.5mm, M3 (3.05mm)

## Specifications

| Feature                | Details                                                                                         |
|------------------------|-------------------------------------------------------------------------------------------------|
| Power Input            | 2-6S Lipo (6-26V)                                                                               |
|                        |                                                                                                 |
| VOXL Power Output      | 5V @ 6A                                                                                         |
| VTX Power Output       | 16.8V @ 500mA (switch on/off). Passthrough if Vbatt < 16.8                                      |
| AUX Power Output       | 3.3V / 5.0V @ 500mA (switch on/off, voltage selectable in software)                             |
| Payload Connector      | Battery Voltage, JST Connector, rated 4A                                                        |
|                        |                                                                                                 |
| Performance            | 40V, 100A+ Mosfets                                                                              |
|                        | 100A peak current per motor (<1s)                                                               |
|                        | 40A continuous current per motor (with sufficient airflow, application dependent)               |
|                        | Peak total board current 300A+ (<1s)                                                            |
|                        |                                                                                                 |
| Software Features      | Open-loop control (set desired % power)                                                         |
|                        | Closed-loop RPM control (set desired RPM), used in PX4 driver                                   |
|                        | Regenerative braking                                                                            |
|                        | Smooth sinusoidal spin-up                                                                       |
|                        | Tone generation using motors                                                                    |
|                        | Real-time RPM, temperature, voltage, current feedback via UART                                  |
| Communications         | Supported by VOXL 2, VOXL 2 Mini, VOXL Flight, VOXL and Flight Core                             |
|                        | Bi-directional UART up to 2Mbit/s (3.3VDC logic-level)                                          |
|                        |                                                                                                 |
| Connectors             | 4-pin JST GH for UART communication                                                             |
|                        | VOXL output: solder pads for 5.0V / 3.8V (depends on board variant)                             |
|                        | Payload connector: JST SM02B-SFHLS-TF(LF)(SN)                                                   |
|                        | VTX power output: solder pads                                                                   |
|                        | AUX power output: solder pads                                                                   |
|                        |                                                                                                 |
| Hardware               | MCU : STM32F051K86 @ 48Mhz, 64KB Flash                                                          |
|                        | Mosfet Driver : MP6531                                                                          |
|                        | Mosfets: TBD (N-channel)                                                                        |
|                        | Current Sensing : 0.25mOhm + INA186 (total current only). 200A max sensing range                |
|                        | ESD Protection : Yes (on UART and PWM I/O)                                                      |
|                        | Temperature Sensing : 4x internal to MCU, 2x external (top and bottom of PCB)                   |
|                        | On-board Status LEDs : Yes                                                                      |
|                        | Weight (no wires) : 17.9g                                                                       |
|                        | Motor Connectors: N/A (solder pads)                                                             |
| PX4 Integration        | Supported in PX4 1.12 and higher                                                                |
|                        | Available [here in developer preview](https://github.com/modalai/px4-firmware/tree/modalai-esc) |
| Resources              | [Manual](/modal-esc-v2-manual/)                                                                 |
|                        | [PX4 Integration User Guide](/modal-esc-px4-user-guide/)                                        |

## Wiring Diagrams

### M0138-1 FPV ESC with Flight Core v2

![m0138-flight-core-wiring.jpg](/images/modal-esc/m0138/m0138-flight-core-wiring.jpg)
