---
layout: default
title: Orqa / GHST
parent: Transmitters
nav_order: 10
has_children: false
permalink: /orqa-ghost/
---

# Orqa/GHST Setup
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

<br>
<br>




- [Here is a general link](https://orqafpv.freshdesk.com/support/home) to the Orqa help website if something isn't on this page.
- [This is a link](https://www.immersionrc.com/?download=6746) to the ImmersionRC Ghost Manual, which seems to cover everything Ghost related. [Here is a pdf](/docs/70-accessories/35-transmitters/files/Ghost-Manual-v2.5.pdf) of the same thing.
- The [quickstart guide](/docs/70-accessories/35-transmitters/files/Ghost-Quick-Start-Guide.pdf) is also useful.





## **Orqa FPV.Ctrl with Ghost UberLite Module**

<br>

<img src="/docs/70-accessories/35-transmitters/files/OrqaTransmitterA.png" style="width:600px;" >


### **Key points: Updating, Pairing, Binding**

This [FPV.ctrl Manual](/docs/70-accessories/35-transmitters/files/FPV.CTRL User Manual Rev.1.1.pdf) is a good source if something is not yet explained here.

<br>

#### **Updating**
{: .no_toc }

- Orqa has a guide for this already, hosted on [this Google Doc.](https://docs.google.com/document/d/14SjmFIHuKUN89edHd-gWJAc4zYP3Fi-y2eGyb7V_i3c/edit) [Here is a pdf download as well (version 1.0.8.3)](/docs/70-accessories/35-transmitters/files/Readme.pdf)
- The Orqa Transmitter has a separate firmware from the UberLite Module, and that is the only thing able to be updated through the app. As of 02/2024, the current version for the Orqa Transmitter is 1.0.4.  To get the up to date firmware on the UberLite Module, you will need to follow the steps in the pdf above.

<br>

#### **Pairing to App**
{: .no_toc }

- Will depend on your device of choice, connect your transmitter to your device via bluetooth. The app should walk you through this once opened.

<br>

#### **Binding**
{: .no_toc }

- Download Orqa FPV.ctrl app from your phone or tablet’s app store

- Open app, connect to transmitter, and then enter the “Ghost Menu”

- If this is a new receiver, it will automatically be in bind mode. To get a previously bound (to a different transmitter) receiver into bind mode, power up the receiver and then press the button next to the antenna connector within 30 seconds.  The light should turn blue to indicate searching for transmitter.

- Go through the settings and choose which modes you want, then click “Bind”

- In this menu, choose which protocol you want, it is recommended to use GHST. Rx ID does not matter unless you will have multiple drones flying at once.

- Click on “Start Bind.” After a brief moment it should say “Success”

- Congratulations, you are now bound. It is recommended to restart the drone and transmitter/app at this point.

<br>

- **Alternative Bind**: If you do not want to use the app and do not have a need to change modes or settings of any sort, you can power on the Orqa transmitter, then press it’s button on the back (On the antenna module) and it should turn blue, indicating binding mode.  Couple this with the binding mode of the receiver and they should both go through a series of colors and flashes before turning solid green each. 

<br>





## **ImmersionRc Ghost xLite Module**

<img src="/docs/70-accessories/35-transmitters/files/xLiteModuleHeader.jpeg" style="width:500px;" >

### **Key points: Installing, Updating, Binding**

#### **Installing**:
{: .no_toc }

- This can be installed in transmitters with Lite module bays such as the Taranis X9 Lite or TBS Tango 2 (with module bay mod). Each transmitter is different, but will usually require you to remove the plastic cover of the external module bay, and then slide in the Ghost xLite Module. That is the installation.

- To use the GHST Protocol, the transmitter will need to be updated to at least [OpenTX](https://www.open-tx.org/downloads) 2.3.10, which is the first firmware to support GHST transmission. (As of 02/2024, most up to date firmware is 2.3.15). Again, this updating process is entirely dependent on what transmitter you have. Most have detailed guides online.

- Ghost is fully supported in 2.3.13 and later.

- If not using a firmware at or beyond 2.3.10, SBus may be used on 2.3.09 and below by selecting SBus in the External Module protocol setting.

#### **Updating**:
{: .no_toc }

- To update the module, it must be connected to a Windows or MacOS computer with the Ghost Updater/Installer installed.  A link to download what will hopefully be the most up to date installer can be found in the “Firmware/Downloads” Tab, then “Utility Software” section near the bottom of [This Page](https://www.immersionrc.com/fpv-products/ghost/#specifications).

- On Windows, download (From the same place as above) and install the drivers, then open the installer. As of this guide, 1.8.0 is the latest version. 

- For Mac, download the installer, no drivers necessary.

- Plug in the Module via usb, run the installer and verify the program sees the module on the correct COM port, this was automatic for me. Then click the ‘Update Ghost’ button. It should automatically have the latest firmware selected if connected to the internet. <br>
<img src="/docs/70-accessories/35-transmitters/files/GhostUpdaterA.png" style="width:750px;" >

- It will take a minute or two to finish the update, then give a success message. <br>
<img src="/docs/70-accessories/35-transmitters/files/GhostUpdaterB.png" style="width:750px;" >

- This will also give the module the ability to update any receivers it connects to the latest firmware as well. 

- If you have already bound to receivers with this module, rebind to them to update their software.  If a receiver is on an older version, it will automatically update when binding. (This at one point was a separate step, but has been since streamlined and no longer needs that).

<br>

#### **Binding**
{: .no_toc }

- The Ghost PDF explains its capabilities well: <br>
<img src="/docs/70-accessories/35-transmitters/files/BindingImg.png" style="width:750px;" >





## **ImmersionRC Ghost JR Module**

<br>

<img src="/docs/70-accessories/35-transmitters/files/GhostJrModuleHeader.jpeg" style="width:500px;">

<br>

This is nearly identical to the xLite module, but fits into larger module bays and has a larger screen.  Software and navigation are the same, with exception to settings involving the use of the two antennas compared to the xLite’s one.  Installation should be the same as any other JR Module, simply remove the bay cover and push the module into the bay.

<br>

## **ImmersionRC Ghost Atto Receiver**

<br>

<img src="/docs/70-accessories/35-transmitters/files/GhostAttoHeader.jpeg" style="width:500px;">

This is the main Ghost receiver we use and will bind with any of the above systems.

<br>

### **Connecting to Voxl/Flight Core**

#### **Voxl 2**
{: .no_toc }

- Make sure you have the receiver connected to the proper port and pins (See “Pinout” below)

- Before it will communicate with the Voxl, there will need to be a change in the “voxl-px4.conf” file
    - RC_Input needs to be set to “GHST” if it isn’t already.  This file lives on the Voxl in /etc/modalai/

#### **Voxl Flight / Flight Core V1/ Flight Core V2**
{: .no_toc }

- For Voxl-Flight:
    - Use MSA-D0001-1-02, plugged into J1003 and J1004, as shown here:
    <img src="/docs/70-accessories/35-transmitters/files/VoxlFlightCoreA.png" style="width:500px;">
    
    - Select “GHST” in QGroundControl under the “RC_INPUT_PROTO” parameter.

    - Pinout is as follows: (Connects to)
        - J1003 Pin 1- 5VDC (“+” on Atto)
        - J1003 Pin 3- GND (“G” on Atto)
        - J1004 Pin 2- USART6_TX (“S” on Atto)

<br>

- For Flight Core V1 and V2:
    - Not yet documented, coming soon.

<br>

## **Extra Information**

### **Updating, Pinout, LED Indicator Lights, Atto Dimensions, Misc**

<br>

#### **Updating**
{: .no_toc }

- If using any of the above transmitters, the Atto, Atto Duo, and Zepto receivers should all automatically check and update (if necessary) on a bind to an updated transmitter.

<br>

#### **Pinout**
{: .no_toc }

- This is the Pinout for the Atto, it is the same on the Zepto and Atto Duo.

- The Atto has one U.FL antenna, Duo has two U.FL antennas and the Zepto has a standard MHF4 antenna.  All of them are 2.4gHz.

- Connecting to a Voxl 2 should be as follows: **Gnd to Gnd**, **5v to 5v** (It may tolerate 3.3v at potentially reduced performance, ModalAi has **not** tested this.) and **Serial Out to Rx**. 

- We have not had any issues with having **Tramp Telemetry** connected to **Tx**, which allows utilization of ELRS receivers as well without swapping cables if connecting via MCBL-89-01 using the plug and play [Harwin Rectangular Connector](https://www.digikey.com/en/products/detail/harwin-inc/M20-1060400/3728193). 

- ModalAi drones will only come with the Gnd, 5v, and S connected. T will be open.

<br>

<img src="/docs/70-accessories/35-transmitters/files/AttoPinoutA.png" style="width:1000px;" >

<br>

#### **LED Indicator Lights:**
{: .no_toc }

<br>

<img src="/docs/70-accessories/35-transmitters/files/ReceiverLEDTable.png" style="width:1000px;" >

<br>

#### **RF Modes:**
{: .no_toc }

- ModalAi suggests using Race Mode for optimal performance (while still retaining some range and telemetry) in non long range scenarios.  In some edge case use, normal mode will ‘lag' a bit as it uses a much lower frame rate. 

<img src="/docs/70-accessories/35-transmitters/files/RFModes.png" style="width:1000px;" >

<br>

#### **Physical Dimensions of Atto:**
{: .no_toc }

<br>

<img src="/docs/70-accessories/35-transmitters/files/AttoDimensions.png" style="width:1000px;">

<br>

#### **Miscellaneous:**
{: .no_toc }

- Something ran into during testing is a short or total removal between two small pads between the 5v and Gnd pads. If these are messed up, the receiver will flash Red and then Yellow multiple times on repeat.  This has been documented on [reddit](https://www.reddit.com/r/Multicopter/comments/mr8wym/immersionrc_ghost_atto_rx_undocummented_led/) by hobbyists. <br>
<img src="/docs/70-accessories/35-transmitters/files/RemovedFerriteFilter.png" style="width:750px;" >
