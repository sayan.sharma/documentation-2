---
layout: default3
title: M0062 VOXL 2 Ethernet Expansion and USB Hub
nav_order: 17
parent: Expansion Boards
has_children: true
permalink: /m0062-2/
buylink: https://www.modalai.com/products/m0062-2
summary: VOXL 2 Developer Test Board (M0144)
---

# VOXL 2 Ethernet Expansion and USB Hub

{: .no_toc }

## Overview

The M0061-2 VOXL 2 Ethernet Expansion and USB Hub provides convenient access to extra I/O.

_Note: the USB A connector, J10, is not working consistently on this board. We are investigating this matter and in the meantime, please use J11 if you need USB. You can use MCBL-00022-2 to convert this into an "A-type" port._

## Details
