---
layout: default
title: VOXL 2 Developer Test Board Developer Guide
parent: VOXL 2 Developer Test Board
nav_order: 16
has_children: true
permalink: /voxl2-dev-test-board-developer-guide/
---

# VOXL 2 IO Developer Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview

The VOXL 2 Developer Test Board (M0144) is the first of-its-kind plug-in expansion board that allows Hardware and Software developers of the VOXL 2 ecosystem to access and utilize all GPIO/QUP signals (and others) from J3 and J5.


TO DO
