---
layout: default
title: Starling
nav_order: 30
has_children: true
permalink: /starling/
parent: Dev Drones
summary: Starling is our flagship development drone for both indoor and outdoor flight.
thumbnail: /starling-v2/starling-v2-hero-1.png
buylink: https://www.modalai.com/starling
---

# Starling
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview

Starling is our flagship development drone for both indoor and outdoor flight.


<a href="https://www.modalai.com/products/starling?variant=45182917640496" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>

![Starling](/images/starling-v2/starling-v2-hero-2.png)

### Video Overview

{% include youtubePlayer.html id="MGJwPyVU71g" %}

### Starling Running voxl-mapper

{% include youtubePlayer.html id="gqlSKRP8prc" %}


