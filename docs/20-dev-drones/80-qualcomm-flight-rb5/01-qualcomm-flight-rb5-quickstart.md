---
layout: default
title: Qualcomm Flight RB5 Quickstart
parent: Qualcomm Flight RB5 
nav_order: 1
has_children: False
youtubeId: zAPg070k8do 
permalink: /Qualcomm-Flight-RB5-quickstart/
search_exclude: true
---

# Qualcomm Flight RB5 Reference Drone Quickstart
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## VOXL SDK for R5B Flight

{: .alert .simple-alert}
**Update 2022-07-20:** Qualcomm Flight RB5 vehicle is now supported by the VOXL SDK!  Please see the [upgrade guide here](/Qualcomm-Flight-RB5-voxl-sdk-upgrade-guide/) from more information


## Overview

The [User's Guide](/Qualcomm-Flight-RB5-user-guides/) walks through taking the Qualcomm Flight RB5 out of the box and up into the air!

For technical details, see the [datasheet](/Qualcomm-Flight-RB5-datasheet/).

<br>
{% include youtubePlayer.html id=page.youtubeId %}


{: .alert .simple-alert}
**WARNING:** *Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*

---

### Required Materials

To follow this user guide, you'll need the following:

- Spektrum Transmitter 
  - e.g. SPMR6655, SPM8000 or SPMR8000
  - Any Spektrum transmitter with DSMX/DSM2 compatibility will likely work
  - Buy [Here](https://www.modalai.com/products/m500-spektrum-dx6e-pairing-and-installation?_pos=2&_sid=8af1d1b9b&_ss=r) 
- Wall Power Supply or Battery with XT60 connector
  - e.g. Gens Ace 3300mAh, or any 4S battery with XT60 connector
  - Buy Battery [Here](https://www.modalai.com/products/4s-battery-pack-gens-ace-3300mah) 
  - Buy Wall Power Supply [Here](https://www.modalai.com/products/ps-xt60?_pos=1&_sid=f5b241f03&_ss=r)  
- Host computer with:
  - QGroundControl 3.5.6+
  - Ubuntu 18.04+
  - Wi-Fi

<hr>

### What's in the Box

Drone Only:

- Qualcomm Flight RB5 Development Kit with Spektrum Receiver
- 10" props
- 5G Modem (Optional)

![RB5-drone-only](/images/rb5/MRB-D0004-3-V1-F1-B1-C11-M7-T1-G0-K0-FINAL.jpg)
<hr>
Basic Dev Kit:

- Qualcomm Flight RB5 Development Kit with Spektrum Receiver
- 10" props
- 5G Modem (Optional)
- Desktop Development Fan
- Power Supply

![RB5-basic-kit](/images/rb5/MRB-D0004-3-V1-F1-B1-C11-M7-T1-G0-K1-FINAL.jpg)
<hr>

Pro Dev Kit:

- Qualcomm Flight RB5 Development Kit with Spektrum Receiver
- (2 Pair) 10" props
- 5G Modem (Optional)
- Desktop Development Fan
- Power Supply
- Spektrum DX6e Transmitter
- Hard Case

![RB5-pro-kit](/images/rb5/MRB-D0004-3-V1-F1-B1-C11-M7-T1-G0-K2-FINAL.jpg)

