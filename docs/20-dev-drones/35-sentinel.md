---
layout: default
title: Sentinel
nav_order: 35
has_children: true
permalink: /sentinel/
parent: Dev Drones
summary: Sentinel combines a premium-tier companion computer and flight controller into a single SWAP-optimized PCB to enable smaller, smarter, and safer drones and robots
thumbnail: /sentinel/sentinel-front-clear.png
buylink: https://www.modalai.com/pages/sentinel
---

# Sentinel
{: .no_toc }

A comprehensive drone reference design with low-power, high-performance heterogenous computing, artificial intelligence engine that is designed to deliver 15 TOPS deep learning capability, GPS-denied navigation, obvstacle avoidance and 5G connectivity, support for 7 image sensor concurrency, computer vision, and vault-like security. 

<a href="https://www.modalai.com/pages/sentinel" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/33/sentinel" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>



![Sentinel](/images/sentinel/sentinel-front-clear.png)
