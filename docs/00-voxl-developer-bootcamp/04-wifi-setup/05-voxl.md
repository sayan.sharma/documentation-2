---
layout: default
title: 4.2 VOXL WiFi
parent: 4. WiFi Setup
nav_order: 10
permalink: /voxl-wifi-setup/
has_children: false
---

# VOXL WiFi Setup

## Overview
This page serves as a WiFi setup guide for all devices based on the original VOXL architecture including the VOXL and the VOXL Flight. Since the VOXL Flight is just a VOXL with an attached Flight Core, all instructions in this section are the same for both devices.

## Hardware Setup
- Disconnect power from the device
- Attach Wi-Fi antennas as shown below (note it takes a little force to connect the MHF4 U.FL connectors)
- Power up the device

![VOXL Wi-Fi](/images/voxl/wifi-antennas.png)

You can also see a demonstration of this attachment in the VOXL Flight Quickstart video at minute 1:36:

{% include youtubePlayer.html id="Kjw7X6WKDUw" %}

## Configuration and Connection
All devices support two WiFi modes, Station and SoftAP. Station Mode refers to the device being configured into a typical 'WiFi client' on a wireless network like your phone or laptop. Software enabled Access Point Mode (SoftAP) configures the device to generate its own hotspot so that other WiFi devices can connect to it. This is useful if you are in the field away from your home or office WiFi network. Switching between the two modes can be done at any time using the `voxl-wifi` command inside an ADB shell.

### Configure and Connect in Station Mode

To configure the VOXL device into Station Mode, run the following command from the host machine:

```bash
me@mylaptop:~$ adb shell voxl-wifi station <SSID> <Password>
```

To finish the setup, reboot the device:

```bash
me@mylaptop:~$ adb reboot && adb wait-for-device
```

Once the device has rebooted, you can use the command `ifconfig` to get the IP address of your device on the network like:

```bash
me@mylaptop:~$ adb shell ifconfig
lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:32 errors:0 dropped:0 overruns:0 frame:0
          TX packets:32 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:2224 (2.1 KiB)  TX bytes:2224 (2.1 KiB)

wlan0     Link encap:Ethernet  HWaddr 48:5F:99:9D:40:F7  
          inet addr:192.168.1.92  Bcast:192.168.1.255  Mask:255.255.255.0
          inet6 addr: fe80::4a5f:99ff:fe9d:40f7/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:690 errors:0 dropped:1 overruns:0 frame:0
          TX packets:14 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:3000 
          RX bytes:225347 (220.0 KiB)  TX bytes:1358 (1.3 KiB)
```

The IP address can be found from the `inet addr` which in this case is `192.168.1.92`. Now you can connect via `ssh` as:

```bash
me@mylaptop:~$ ssh root@192.168.1.92
```
Where here the IP address following `root@` should correspond to the above output from `ifconfig`. The password to connect will be `oelinux123`. 

*Note:* It's not currently possible to assign a static IP, only DHCP is supported. If you need a static IP on your own wifi network it's best to configure that in your router's software.

### Configure and Connect in SoftAP Mode
By default, the unit ships configured in SoftAP mode with an SSID of `VOXL-XX:XX:XX:XX:XX:XX` with password `1234567890` (where XX:XX:XX:XX:XX:XX is the unit's MAC address. To configure the VOXL into Access Point Mode with your desired SSID, use the `voxl-wifi` utility:

```bash
me@mylaptop:~$ adb shell voxl-wifi softap <SSID>
```

To finish the setup, reboot the device:

```bash
me@mylaptop:~$ adb reboot && adb wait-for-device
```

Now on your host machine you can connect to the SSID specified above using the password `1234567890`. The VOXL will assign itself the IP `192.168.8.1` and so from your host machine you can use `ssh` to connect as:

```bash
me@mylaptop:~$ ssh root@192.168.8.1
```

Your device will be assigned an IP with DHCP in the 192.168.8.X subnet, usually 192.168.8.60, but not always.

<br>
[Next: Inspect and Configure SKU ](/sku/){: .btn .btn-green }


