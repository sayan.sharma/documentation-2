---
layout: default
title: Inspect Tools 0.9
parent: Modal Pipe Architecture 0.9
search_exclude: true
nav_order: 15
has_children: true
has_toc: true
permalink: /inspect-tools-0_9/
---

# Inspect Tools

Most pipes in Modal Pipe Architecture have some sort of `voxl-inspect-xxx` tool to allow easy debug and visualization of data.

Most of these tools are part of the [voxl-mpa-tools project on GitLab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/tools/).
