---
title: Feature Guides 
layout: default
parent: VOXL SDK
has_children: true
nav_order: 15
permalink: /high-level-features/
---


# High Level Features
{: .no_toc }

## Overview

These are the primary features and functions provided by the [VOXL SDK](/voxl-sdk/) which are designed as starting points to help you accelerate OEMs in the development of their own final products.