---
title: Config Conventions
layout: default
parent: SDK Configuration
has_children: false
nav_order: 02
permalink: /mpa-configuration-conventions/
---

# SDK Configuration Conventions

All [MPA](/mpa/) services have configuration files in JSON format in `/etc/modalai/` and use [libmodal_json](/libmodal-json/) to read and write these files. The /etc/ directory is wiped during a system image flash, ensuring MPA is returned to a known state when reflashed.

These configuration files are "self-maintaining" in the sense that during software updates, new config entries to support new features are automatically added to the config files by the service itself.

Every service has a complementary `voxl-configure-xyz` bash script that can be used to enable or disable the service, as well as reset it to factory defaults. For a list of these config scripts, run:


```bash

voxl2:/$ ls /usr/bin/voxl-configure-*
/usr/bin/voxl-configure-cameras          /usr/bin/voxl-configure-mavcam          /usr/bin/voxl-configure-qvio
/usr/bin/voxl-configure-cpu-monitor      /usr/bin/voxl-configure-mavlink-server  /usr/bin/voxl-configure-remote-id
/usr/bin/voxl-configure-dfs              /usr/bin/voxl-configure-modem           /usr/bin/voxl-configure-sku
/usr/bin/voxl-configure-docker-support   /usr/bin/voxl-configure-mpa             /usr/bin/voxl-configure-streamer
/usr/bin/voxl-configure-extrinsics       /usr/bin/voxl-configure-pkg-manager     /usr/bin/voxl-configure-tag-detector
/usr/bin/voxl-configure-feature-tracker  /usr/bin/voxl-configure-portal          /usr/bin/voxl-configure-tflite
/usr/bin/voxl-configure-flow-server      /usr/bin/voxl-configure-px4             /usr/bin/voxl-configure-uvc
/usr/bin/voxl-configure-imu              /usr/bin/voxl-configure-px4-imu-server  /usr/bin/voxl-configure-vision-hub
/usr/bin/voxl-configure-lepton           /usr/bin/voxl-configure-px4-params
```


Each service has different options, but they all follow a common theme. For example, `voxl-configure-px4-params` is extremely elaborate with a wizard that helps the user configure PX4 parameters for different airframes, sensors, and flight behaviors.

On the other end, voxl-cpu-monitor is a very simple service that can either be on or off. The help text for voxl-configure-cpu looks like this:

```bash
voxl2:/$ voxl-configure-cpu-monitor --help

Start wizard with prompts:
voxl-configure-cpu-monitor

Shortcut configuration arguments for scripted setup.
factory_enable will reset the config file to factory defaults
before enabling the service.

voxl-configure-cpu-monitor disable
voxl-configure-cpu-monitor factory_enable
voxl-configure-cpu-monitor enable

show this help message:
voxl-configure-cpu-monitor help

```


These individual configure scripts are called in a particular sequence by [voxl-configure-mpa](/voxl-configure-mpa/) to set up all services.

[Next: voxl-configure-mpa](/voxl-configure-mpa/){: .btn .btn-green }
