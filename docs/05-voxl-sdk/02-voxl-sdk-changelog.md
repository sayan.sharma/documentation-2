---
title: VOXL SDK Release Notes
layout: default
parent: VOXL SDK
has_children: true
nav_order: 02
permalink: /voxl-suite/
---


# VOXL SDK Release Notes
{: .no_toc }


voxl-suite is the meta package providing all VOXL SDK software packages that are installed on VOXL. voxl-suite is bundled up with a matching [system image](/voxl2-system-image/) and an installer to create an [SDK Release](/upgrading-sdk-version/).

A [VOXL SDK Release](/upgrading-sdk-version/) its version number from the included voxl-suite meta package.

It is highly recommended to stick to using [SDK Releases](/upgrading-sdk-version/) as-is since they are tested as a whole with System Image and voxl-suite together. If you are an experienced developer and know what you are doing then you can follow the instructions on the [voxl-configure-pkg-manager page](/configure-pkg-manager/) to configure VOXL to pull packages from development repositories, but we will not support software issues that arise from this.

Patch version bumps, e.g. V1.0.0 to V1.0.1 do not require a system image upgrade and can be upgrading in place with `apt update && apt upgrade`. However, minor version bumps, e.g. V1.0.0 to V1.1.0 will require a complete [system flash](/upgrading-sdk-version/).


