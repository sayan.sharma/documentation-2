---
layout: default
title: VOXL 2 Mini USB Connections
parent: VOXL 2 Mini User Guides
nav_order: 3
permalink:  /voxl2-mini-usb-connections/
---

# VOXL 2 Mini USB Connections
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

There are two USB interfaces.

|--- |--- |
| J3 | a 10-pin JST-GH USB3 interface.  Note: you can use the lower 4 pins only for a USB2 interface and reduce wire count.  For USB3, see [MCBL-00022-2](/cable-datasheets/#mcbl-00022) |
| J9 | a USBC interface, typically used to access the ADB shell.  Note: this can also be used as a USB Host for adding debug peripherals like ethernet adapters for benchtop use |

<img src="/images/voxl2-mini/m0104-user-guides-usb.jpg" alt="m0104-user-guides-usb" width="1280"/>

## J3 - USB3 10-Pin JST

[MCBL-00022-2](/cable-datasheets/#mcbl-00022) is recommended.

Example using a wired ethernet adapter connected to J3 ([Cable Creation USB to Ethernet](https://www.amazon.com/Network-Adapter-CableCreation-Ethernet-Supporting/dp/B013G4C8RE))

<img src="/images/voxl2-mini/m0104-mcbl-00022-2-J3-0.jpg" alt="m0104-mcbl-00022-2-J3-0.jpg" width="1280"/>

Example using a wireless ethernet adapter connected to J3 (Alfa Network AWUS036ACS 802.11ac AC600)

<img src="/images/voxl2-mini/m0104-mcbl-00022-2-J3-1.jpg" alt="m0104-mcbl-00022-2-J3-1.jpg" width="1280"/>

## J9 - Debug USB

### J9 - Primary - ADB Inteface

The primary use case for 'J9' is for gaining shell access using ADB.  See [VOXL 2 Mini Shell Access](/voxl2-mini-shell-access/) for more information.

It is recommended to use a USBC to USB type A cable, and we like flexible cables to prevent too much torque on the J9 connector introduced by thicker, inflexible cables.

<img src="/images/voxl2-mini/m0104-j9-usbc-to-usba.jpg" alt="m0104-j9-usbc-to-usba" width="1280"/>

### J9 - Secondary - USBC Host Mode

Note: we recommend using this for debug purposes, and don't recommend using this port in final product as it prevents you from access to perform system updates.

Note: shown is a USBC to USBA (as these were the peripherals available to serve as an example, but direct USBC also are OK)

<img src="/images/voxl2-mini/m0104-mcbl-00022-2-J9-0.jpg" alt="m0104-mcbl-00022-2-J9-0.jpg" width="1280"/>

<img src="/images/voxl2-mini/m0104-mcbl-00022-2-J9-1.jpg" alt="m0104-mcbl-00022-2-J9-1.jpg" width="1280"/>




