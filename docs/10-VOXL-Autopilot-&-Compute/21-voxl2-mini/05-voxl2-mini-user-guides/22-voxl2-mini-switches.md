---
layout: default
title: VOXL 2 Mini Switches
parent: VOXL 2 Mini User Guides
nav_order: 22
permalink: /voxl2-mini-switches/
---

# VOXL 2 Mini Switches
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

Below describes the VOXL 2 Mini switches.

<img src="/images/voxl2-mini/m0104-datasheets-switches.jpg" alt="m0104-datasheets-switches" width="1280"/>

### SW1 - Force Fastboot Button

Force Fastboot momentary button.

To force device into fastboot mode:

- power off device, remove USB cable to completely power down
- press and hold SW1 button down
- power on device, attach USB cable
- release SW1 button
- from host computer, run `fastboot devices` and verify the device shows up.  If not, restart this procedure

To reboot device to fastboot:

- device is powered on
- press and hold SW1 for 30 seconds until the device reboots into fastboot mode

<hr>

### SW2 - EDL Switch

Emergency Download switch, used for factory flashing.  Should be left `OFF`.  See [user guide for QDL](/voxl2-unbricking/) if interested in more information.
